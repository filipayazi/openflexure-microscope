# Attach the sample clips


{{BOM}}

## Attaching the sample clips {pagestep}

* Take one [sample clip][Sample clips](fromstep){qty:1} and place it over a hole on the illumination side of the stage
* Fasten it with an [M3x10 cap screw][M3x10 cap head screws](parts/mechanical.yml#CapScrew_M3x10mm_SS){qty: 2, cat:mech}
* Repeat for the other clip