* [Separate z-actuator]{output,qty:1}: [separate_z_actuator.stl](models/separate_z_actuator.stl){previewpage} - The smart brim may require [custom print settings].
* [Upright z-actuator mount]{output,qty:1}: [upright_z_actuator_mount.stl](models/upright_z_actuator_mount.stl){previewpage}
* 4 [feet]{output,qty:4}: [upright_feet.stl](models/upright_feet.stl){previewpage}
* 4 [large gears]{output,qty:4}: [upright_large_gears.stl](models/upright_large_gears.stl){previewpage}
* [Upright condenser]{output,qty:1}: [upright_condenser.stl](models/upright_condenser.stl){previewpage} - The condenser may require [custom print settings] as used for the smart brim.
* [Lens spacer]{output,qty:1}: [lens_spacer_picamera_2_pilens.stl](models/lens_spacer_picamera_2_pilens.stl){previewpage} - **This must be printed in [black][Black PLA filament]{Qty: 50g}!** [i](info_pages/why_optics_black.md)
* [pi camera platform]{output,qty:1}: [camera_platform_picamera_2_pilens.stl](models/camera_platform_picamera_2_pilens.stl){previewpage}

You can [download all of the STLs as a single zipfile](upright-stls.zip){zip, pattern:"*.stl"}

### Optional accessories

Taller upright z-actuator mounts are available for use with thicker samples:

* [accessories/upright_z_actuator_mount_5mm_sample.stl](models/accessories/upright_z_actuator_mount_5mm_sample.stl){previewpage}
* [accessories/upright_z_actuator_mount_10mm_sample.stl](models/accessories/upright_z_actuator_mount_10mm_sample.stl){previewpage} 

[Black PLA filament]: parts/materials/black_pla_filament.md "{cat:material}"