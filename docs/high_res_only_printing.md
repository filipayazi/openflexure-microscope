* [Illumination dovetail]{output,qty:1}: [illumination_dovetail.stl](models/illumination_dovetail.stl){previewpage}
* [Condenser arm]{output,qty:1}: [condenser.stl](models/condenser.stl){previewpage}
* [Illumination thumbscrew]{output,qty:1}: [illumination_thumbscrew.stl](models/illumination_thumbscrew.stl){previewpage}
* 3 [large gears]{output,qty:3}: [large_gears.stl](models/large_gears.stl){previewpage}
* 3 [feet]{output,qty:3}: [feet.stl](models/feet.stl){previewpage}
* [Optics module]{output,qty:1}: [optics_picamera_2_rms_f50d13.stl](models/optics_picamera_2_rms_f50d13.stl){previewpage} - **This must be printed in [black][Black PLA filament]{Qty: 50g}!** [i](info_pages/why_optics_black.md)
* [pi camera cover]{output,qty:1}: [picamera_2_cover.stl](models/picamera_2_cover.stl){previewpage}

You can [download all of the STLs as a single zipfile](high-res-stls.zip){zip, pattern:"*.stl"}

[Black PLA filament]: parts/materials/black_pla_filament.md "{cat:material}"