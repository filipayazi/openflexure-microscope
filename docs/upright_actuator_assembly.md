## Repeat this process for separate z-actuator {pagestep}

[Separate z-actuator][prepared separate z-actuator](fromstep){qty:1, cat:subassembly}. Once complete the [complete separate z-actuator]{output, qty:1} should look like this:

![](images/upright/z-axis_with_foot.jpg)
![](images/upright/z-axis_with_foot_underneath.jpg)
