#!/usr/bin/env python3

"""
This is the main script to create the renderings used in the documentation.
"""

# Function docstrings are fairly redundant in this file
# pylint: disable=missing-function-docstring

import os
from build_system.openscad_render_system import RenderSystem, ScadRender, Camera

def register_rms_optics_assembly(rendersystem):
    input_file = "rendering/rms_optics_assembly.scad"
    cameras = []
    imgsizes = []
    scad_lines = []
    output_files = []

    for frame in [1, 2, 3]:
        cameras.append(Camera(position=[29, 0, 59], angle=[69, 0, 90], distance=290))
        imgsizes.append([1000, 2000])
        output_files.append(f"rendering/annotations/optics_assembly_tube_lens{frame}.png")
        scad_lines.append(f"render_rms_assembly({frame});")

    camera_png_files = []
    for frame in [1, 2]:
        cameras.append(Camera(position=[7, -14, -21], angle=[247, 0, 211], distance=250))
        imgsizes.append([1200, 2000])
        output_files.append(f"docs/renders/optics_assembly_camera{frame}.png")
        camera_png_files.append(output_files[-1])
        scad_lines.append(f"render_rms_assembly({frame+3});")

    objective_png_files = []
    for frame in [1, 2]:
        cameras.append(Camera(position=[2, 2, 25], angle=[55, 0, 90], distance=290))
        imgsizes.append([1200, 2000])
        output_files.append(f"docs/renders/optics_assembly_objective{frame}.png")
        objective_png_files.append(output_files[-1])
        scad_lines.append(f"render_rms_assembly({frame+5});")

    screw_png_files = []
    for frame in [1, 2, 3]:
        cameras.append(Camera(position=[-6.5, 14, 38], angle=[60, 0, 243], distance=290))
        imgsizes.append([1000, 2000])
        output_files.append(f"docs/renders/optics_assembly_screw{frame}.png")
        screw_png_files.append(output_files[-1])
        scad_lines.append(f"render_rms_assembly({frame+7});")

    ribbon_png_files = []
    for frame in [1, 2, 3]:
        cameras.append(Camera(position=[12.5, -1, 15], angle=[230, 0, 24], distance=325))
        imgsizes.append([1200, 2000])
        output_files.append(f"docs/renders/optics_assembly_ribbon{frame}.png")
        ribbon_png_files.append(output_files[-1])
        scad_lines.append(f"render_rms_assembly({frame+10});")

    for i, output_file in enumerate(output_files):
        render = ScadRender(output_file, input_file, scad_lines[i], imgsizes[i], cameras[i])
        rendersystem.register_scad_render(render)

    rendersystem.register_imagemagick_sequence(
        "docs/renders/optics_assembly_camera.png",
        camera_png_files
    )
    rendersystem.register_imagemagick_sequence(
        "docs/renders/optics_assembly_objective.png",
        objective_png_files
    )
    rendersystem.register_imagemagick_sequence(
        "docs/renders/optics_assembly_screw.png",
        screw_png_files
    )
    rendersystem.register_imagemagick_sequence(
        "docs/renders/optics_assembly_ribbon.png",
        ribbon_png_files
    )
    rendersystem.register_inkscape_annotation(
        "docs/renders/optics_assembly_tube_lens.png",
        "rendering/annotations/annotate_optics_assembly_tube_lens.svg"
    )

def register_low_cost_optics_assembly(rendersystem):
    input_file = "rendering/low_cost_optics_assembly.scad"
    cameras = []
    imgsizes = []
    scad_lines = []
    output_files = []

    for frame in [1, 2, 3]:
        cameras.append(Camera(position=[29, 0, 59], angle=[69, 0, 90], distance=290))
        imgsizes.append([1000, 2000])
        output_files.append(f"rendering/annotations/low_cost_optics_assembly_tube_lens{frame}.png")
        scad_lines.append(f"render_low_cost_assembly({frame});")

    camera_png_files = []
    for frame in [1, 2]:
        cameras.append(Camera(position=[-5, 7, 25], angle=[71, 0, 98], distance=292))
        imgsizes.append([1200, 2000])
        output_files.append(f"docs/renders/low_cost_optics_assembly_camera{frame}.png")
        camera_png_files.append(output_files[-1])
        scad_lines.append(f"render_low_cost_assembly({frame+3});")

    screw_png_files = []
    for frame in [1, 2, 3]:
        cameras.append(Camera(position=[-11, 19, 33], angle=[71, 0, 106], distance=192))
        imgsizes.append([1000, 2000])
        output_files.append(f"docs/renders/low_cost_optics_assembly_screw{frame}.png")
        screw_png_files.append(output_files[-1])
        scad_lines.append(f"render_low_cost_assembly({frame+5});")

    ribbon_png_files = []
    for frame in [1, 2, 3]:
        cameras.append(Camera(position=[-7, -7, 40.5], angle=[54, 0, 90], distance=237))
        imgsizes.append([1200, 2000])
        output_files.append(f"docs/renders/low_cost_optics_assembly_ribbon{frame}.png")
        ribbon_png_files.append(output_files[-1])
        scad_lines.append(f"render_low_cost_assembly({frame+8});")

    for i, output_file in enumerate(output_files):
        render = ScadRender(output_file, input_file, scad_lines[i], imgsizes[i], cameras[i])
        rendersystem.register_scad_render(render)

    rendersystem.register_imagemagick_sequence(
        "docs/renders/low_cost_optics_assembly_camera.png",
        camera_png_files
    )
    rendersystem.register_imagemagick_sequence(
        "docs/renders/low_cost_optics_assembly_screw.png",
        screw_png_files
    )
    rendersystem.register_imagemagick_sequence(
        "docs/renders/low_cost_optics_assembly_ribbon.png",
        ribbon_png_files
    )
    rendersystem.register_inkscape_annotation(
        "docs/renders/low_cost_optics_assembly_pi_lens.png",
        "rendering/annotations/annotate_optics_assembly_pi_lens.svg"
    )

def register_condenser_assembly(rendersystem):
    input_file = "rendering/condenser_assembly.scad"
    camera = Camera(position=[29, 0, 59], angle=[69, 0, 90], distance=290)
    imgsize = [1000, 2000]

    for frame in [1, 2, 3]:
        scad = f"assemble_condenser({frame});"
        output_file = f"rendering/annotations/optics_assembly_condenser_lens{frame}.png"
        render = ScadRender(output_file, input_file, scad, imgsize, camera)
        rendersystem.register_scad_render(render)
    rendersystem.register_inkscape_annotation(
        "docs/renders/optics_assembly_condenser_lens.png",
        "rendering/annotations/annotate_optics_assembly_condenser_lens.svg"
    )

    camera = Camera(position=[0, 8, 15.5], angle=[62, 0, 130], distance=237)
    imgsize = [2400, 2000]
    for frame in [4, 5, 6]:
        scad = f"assemble_condenser({frame});"
        output_file = f"docs/renders/assemble_condenser_thumbscrew{frame-3}.png"
        render = ScadRender(output_file, input_file, scad, imgsize, camera)
        rendersystem.register_scad_render(render)

def register_optics_assembled(rendersystem):
    input_file = "rendering/optics_assembly.scad"
    camera = Camera(position=[30, 5, 60], angle=[90, 0, 110], distance=440)
    output_file = "docs/renders/optics_assembled.png"
    imgsize = [1200, 2400]
    scad = "cutaway_optics();"
    render = ScadRender(output_file, input_file, scad, imgsize, camera)
    rendersystem.register_scad_render(render)


def register_band(rendersystem):
    input_file = "rendering/band_insertion_cutaway.scad"
    camera = Camera(position=[-13, 13, -30], angle=[76, 0, 216], distance=445)
    imgsize = [1200, 2400]
    png_files = []

    for frame in [1, 2, 3, 4, 5]:
        output_file = f"docs/renders/band{frame}.png"
        scad = f"render_band_insertion(band_insertion_frame_parameters({frame}));"
        png_files.append(output_file)
        render = ScadRender(output_file, input_file, scad, imgsize, camera)
        rendersystem.register_scad_render(render)
    rendersystem.register_imagemagick_sequence("docs/renders/band_instruction.png", png_files)


def register_brim_and_ties(rendersystem):
    input_file = "rendering/brim_and_ties.scad"
    cameras = [
        Camera(position=[9.7, 33, 6], angle=[45.2, 0, 315.2], distance=361),
        Camera(position=[-4, 21, 29], angle=[206, 0, 177], distance=450),
    ]
    imgsize = [2400, 2400]
    for i, camera in enumerate(cameras):
        output_file = f"docs/renders/brim_and_ties{i+1}.png"
        scad = "render_brim_and_ties();"
        render = ScadRender(output_file, input_file, scad, imgsize, camera)
        rendersystem.register_scad_render(render)

def register_prepare_main_body(rendersystem):
    input_file = "rendering/prepare_main_body.scad"
    cameras = [
        Camera(position=[-8, 7, 63], angle=[50, 0, 234], distance=263),
        Camera(position=[-11, 0, 77], angle=[84, 0, 222], distance=155),
        Camera(position=[-11, 0, 77], angle=[84, 0, 222], distance=155),
        Camera(position=[-8, 7, 63], angle=[50, 0, 234], distance=263),
        Camera(position=[-8, 7, 63], angle=[50, 0, 234], distance=263),
        Camera(position=[-8, 7, 63], angle=[50, 0, 234], distance=263),
        Camera(position=[-2, 57, 64], angle=[76, 0, 176.5], distance=179),
        Camera(position=[-2, 57, 64], angle=[76, 0, 176.5], distance=179),
        Camera(position=[-2, 57, 64], angle=[76, 0, 176.5], distance=179),
    ]
    imgsize = [2400, 2000]
    for i, camera in enumerate(cameras):
        output_file = f"docs/renders/prepare_main_body{i+1}.png"
        scad = f"render_prepare_main_body({i+1});"
        render = ScadRender(output_file, input_file, scad, imgsize, camera)
        rendersystem.register_scad_render(render)

def register_prepare_stand(rendersystem):
    input_file = "rendering/prepare_stand.scad"
    cameras = [
        Camera(position=[-9, 20, 37.5], angle=[34, 0, 235], distance=450),
        Camera(position=[-9, 20, 37.5], angle=[34, 0, 235], distance=450),
        Camera(position=[-9, 20, 37.5], angle=[34, 0, 235], distance=450),
        Camera(position=[-9, 20, 37.5], angle=[34, 0, 235], distance=450),
        Camera(position=[-9, 20, 37.5], angle=[34, 0, 235], distance=450),
        Camera(position=[-9, 20, 37.5], angle=[34, 0, 235], distance=450),
    ]
    imgsize = [2400, 2000]
    for i, camera in enumerate(cameras):
        output_file = f"docs/renders/prepare_stand{i+1}.png"
        scad = f"render_prepare_stand({i+1});"
        render = ScadRender(output_file, input_file, scad, imgsize, camera)
        rendersystem.register_scad_render(render)

def register_actuator_assembly(rendersystem):
    input_file = "rendering/actuator_assembly.scad"
    cameras = [
        Camera(position=[2, 5, 14], angle=[33, 0, 242], distance=360),
        Camera(position=[4, 35, 35], angle=[71, 0, 186], distance=330),
        Camera(position=[4, 35, 35], angle=[71, 0, 186], distance=330),
        Camera(position=[4, 35, 35], angle=[71, 0, 186], distance=330),
        Camera(position=[20, 6, 35], angle=[82, 0, 166], distance=500),
        Camera(position=[4, 35, 35], angle=[71, 0, 186], distance=330),
        Camera(position=[4, 35, 35], angle=[71, 0, 186], distance=330),
    ]
    imgsize = [2400, 2000]
    pngs = [
        "actuator_assembly_parts.png",
        "actuator_assembly_nut.png",
        "actuator_assembly_gear.png",
        "actuator_assembly_gear2.png",
        "actuator_assembly_oil.png",
        "actuator_assembly_x.png",
        "actuators_assembled.png",
    ]
    for i, camera in enumerate(cameras):
        output_file = os.path.join("docs/renders/", pngs[i])
        scad = f"render_actuator_assembly({i+1});"
        render = ScadRender(output_file, input_file, scad, imgsize, camera)
        rendersystem.register_scad_render(render)

def register_picam(rendersystem):
    input_file = "rendering/prepare_picamera.scad"
    cameras = [
        Camera(position=[-6, 3, 11], angle=[46, 0, 90], distance=140),
        Camera(position=[0, 0, 0], angle=[29, 0, 90], distance=140),
        Camera(position=[1, 18, 8], angle=[52, 0, 90], distance=140),
    ]
    imgsize = [2400, 2000]
    for i, camera in enumerate(cameras):
        frame = i + 1
        output_file = f"docs/renders/picam{frame}.png"
        scad = f"render_picamera_frame(picam_frame_parameters({frame}));"
        render = ScadRender(output_file, input_file, scad, imgsize, camera)
        rendersystem.register_scad_render(render)

def register_mount_optics(rendersystem):
    input_file = "rendering/mount_optics.scad"
    cameras = [
        Camera(position=[7.75, 37, -3], angle=[135.5, 0, 32.5], distance=495),
        Camera(position=[7.75, 37, -3], angle=[135.5, 0, 32.5], distance=495),
        Camera(position=[7.75, 37, -3], angle=[135.5, 0, 32.5], distance=495),
        Camera(position=[7.75, 37, -3], angle=[135.5, 0, 32.5], distance=495),
        Camera(position=[-18.5, 42.75, 23.75], angle=[57, 0, 143], distance=495),
        Camera(position=[-18.5, 42.75, 23.75], angle=[57, 0, 143], distance=495),
        Camera(position=[-18.5, 42.75, 23.75], angle=[57, 0, 143], distance=495),
        Camera(position=[-18.5, 42.75, 23.75], angle=[57, 0, 143], distance=495),
    ]
    imgsize = [2400, 2000]
    for optics in ["rms", "low_cost"]:
        low_cost = str(optics == "low_cost").lower()
        for i, camera in enumerate(cameras):
            frame = i + 1
            output_file = f"docs/renders/mount_optics_{optics}{frame}.png"
            scad = f"render_mount_optics({frame}, {low_cost});"
            render = ScadRender(output_file, input_file, scad, imgsize, camera)
            rendersystem.register_scad_render(render)

def register_mount_microscope(rendersystem):
    input_file = "rendering/mount_microscope.scad"
    cameras = [
        Camera(position=[24, 43.5, 84], angle=[65.5, 0, 103], distance=550),
        Camera(position=[24, 43.5, 84], angle=[65.5, 0, 103], distance=550),
    ]
    imgsize = [2400, 2000]
    for optics in ["rms", "low_cost"]:
        low_cost = str(optics == "low_cost").lower()
        for i, camera in enumerate(cameras):
            frame = i + 1
            output_file = f"docs/renders/mount_microscope_{optics}{frame}.png"
            scad = f"render_mount_microscope({frame}, {low_cost});"
            render = ScadRender(output_file, input_file, scad, imgsize, camera)
            rendersystem.register_scad_render(render)

def register_mount_illumination(rendersystem):
    input_file = "rendering/mount_illumination.scad"
    cameras = [
        Camera(position=[-6, 49, 178], angle=[68, 0, 133], distance=360),
        Camera(position=[-6, 49, 178], angle=[68, 0, 133], distance=360),
        Camera(position=[-6, 49, 178], angle=[82, 0, 308], distance=360),
        Camera(position=[-6, 49, 178], angle=[82, 0, 308], distance=360),
        Camera(position=[-6, 49, 178], angle=[82, 0, 308], distance=360)
    ]
    imgsize = [2400, 2000]
    for optics in ["rms", "low_cost"]:
        low_cost = str(optics == "low_cost").lower()
        for i, camera in enumerate(cameras):
            frame = i + 1
            output_file = f"docs/renders/mount_illumination_{optics}{frame}.png"
            scad = f"mount_illumination({frame}, {low_cost});"
            render = ScadRender(output_file, input_file, scad, imgsize, camera)
            rendersystem.register_scad_render(render)

def register_motor_assembly(rendersystem):
    input_file = "rendering/motor_assembly.scad"
    camera = Camera(position=[-35, 35, 13], angle=[247, 0, 232], distance=293)
    imgsize = [1200, 2000]
    for i in [1, 2]:
        output_file = f"docs/renders/motor_assembly{i}.png"
        scad = f"motor_assembly({i});"
        render = ScadRender(output_file, input_file, scad, imgsize, camera)
        rendersystem.register_scad_render(render)

def register_mount_motors(rendersystem):
    input_file = "rendering/mount_motors.scad"
    camera = Camera(position=[13.5, 48, 98], angle=[53, 0, 115], distance=360)
    imgsize = [2400, 2000]
    for optics in ["rms", "low_cost"]:
        low_cost = str(optics == "low_cost").lower()
        for i in [1, 2, 3]:
            output_file = f"docs/renders/mount_motors_{optics}{i}.png"
            scad = f"render_mount_motors({i}, {low_cost});"
            render = ScadRender(output_file, input_file, scad, imgsize, camera)
            rendersystem.register_scad_render(render)

def main():
    rendersystem = RenderSystem()
    rendersystem.register_zip_assets('rendering/librender/hardware.zip')
    rendersystem.register_render_stl('rendering/librender/rendered_main_body.scad')
    #Register all openscad renders (and associated post processing)
    register_rms_optics_assembly(rendersystem)
    register_low_cost_optics_assembly(rendersystem)
    register_condenser_assembly(rendersystem)
    register_optics_assembled(rendersystem)
    register_band(rendersystem)
    register_brim_and_ties(rendersystem)
    register_prepare_main_body(rendersystem)
    register_prepare_stand(rendersystem)
    register_actuator_assembly(rendersystem)
    register_picam(rendersystem)
    register_mount_optics(rendersystem)
    register_mount_microscope(rendersystem)
    register_mount_illumination(rendersystem)
    register_motor_assembly(rendersystem)
    register_mount_motors(rendersystem)

    rendersystem.render()

if __name__ == "__main__":
    main()
