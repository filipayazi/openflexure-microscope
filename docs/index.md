# Assembly Instructions
The OpenFlexure Microscope is a 3D printable microscope, with a very precise mechanical translation stage. The microscope is highly customisable. Find about more about the microscope on the [OpenFlexure website](https://openflexure.org/projects/microscope)


These instructions will take you through how to assemble various configurations. The also describe how to make a [Sangaboard Compatible motor controller](nano_sangaboard.md) if you can't get hold of an official one.

## Microscope Configurations:

#### [High-resolution motorised microscope](high_res_microscope.md)
[![](images/MicroscopeBlenderTrio.png)](high_res_microscope.md)  
This configuration of the microscope uses a traditional microscope objective for highest image quality.

#### [Motorised microscope with low-cost optics](low_cost_microscope.md)
This configuration of the microscope uses the original lens from a Raspberry Pi camera module.

#### [Upright microscope](upright-microscope.md)

This configuration of the microscope has the objective above the sample rather than below. It is newer and less well tested than other versions of the microscope. If you have problems building it, please let us know on [GitLab](https://gitlab.com/openflexure/openflexure-microscope/-/issues) or on [our forum](https://openflexure.discourse.group/).
