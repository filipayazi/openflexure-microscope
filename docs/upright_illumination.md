# Assemble the illumination

In this section we are assembling the sample illumination. This is for transmission illumination.

{{BOM}}

[M3 nut]: parts/mechanical.yml#Nut_M3_SS
[M3x25mm stainless steel hex bolt]: parts/mechanical.yml#HexBolt_M3x25mm_SS
[M3 stainless steel washers]: parts/mechanical.yml#Washer_M3_SS
[2.5mm Ball-end Allen key]: parts/tools/2.5mmBallEndAllenKey.md

## Push-fit the lens {pagestep}

* Place the [Condenser lens](parts/optics.yml#CondenserLens){qty:1, cat:optical} on the [lens tool][Lens tool](fromstep){qty:1, cat:printedtool} flat side down
* Take the [upright condenser][Upright condenser](fromstep){qty:1, cat:printedpart} and align the opening over the lens
* Push down until the lens clicks into place

![](images/upright/lens_tool_with_lens.jpg)
![](images/upright/push_fit_lens_apparatus.jpg)
![](images/upright/push_fit_lens.jpg)

## Push-fit the LED {pagestep}

* Push the [LED][LED assembly](fromstep){qty:1, cat:subassembly} as far as possible up the hole in the side of mount of the condenser, it should fit securely

## Add the condenser mounting screw {pagestep}

* Take an [M3 nut]{qty:1, cat:mech} and push it into the nut trap in the condenser mount from the top
* Take an [M3x10 cap head screws](parts/mechanical.yml#CapScrew_M3x10mm_SS){qty:1, cat:mech} and screw it into the nut, only screw a couple of turns about **5mm of thread should still be visible at this stage** 


## Mount the upright condenser onto the main body {pagestep}

* Take the complete upright condenser and pass is through the bottom of the main body until the top of the condenser is in line with the stage.
* Insert the exposed mounting screw into the screw hole in the z-actuator of the main body.
* Insert the [2.5mm Ball-end Allen key]{qty:1, cat:tool} through the teardrop shaped hole on the front of the microscope. Until it engages with the mounting screw.
* Slide the upright condenser up the keyhole until the top of the condenser is 2-4mm below the top of the stage while keeping the Allen key engaged with the screw. 
* Tighten the screw with the Allen key to lock the optics in place.
